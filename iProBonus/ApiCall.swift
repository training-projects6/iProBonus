//
//  ApiCall.swift
//  iProBonus
//
//  Created by Artem Soloviev on 23.06.2023.
//

import Foundation

class ApiCall: ObservableObject {
    
    var postCall = PostCall()
    var  accessString = "" {
        didSet{
            Task{
                await loadData()
            }
        }
    }
    @Published var result:Welcome?
    @Published var dateToStr = ""
    
    init(){
        getAccessToken()
        
    }
    
    func getAccessToken() {
        
        guard let encoded = try?  JSONEncoder().encode(postCall) else {
            print ("Failed to encode post call")
            return
        }
        
        let url =  URL(string: "http://84.201.188.117:5021/api/v3/clients/accesstoken")!
        
        var request =  URLRequest(url: url)
        request.setValue ("891cf53c-01fc-4d74-a14c-592668b7a03c", forHTTPHeaderField: "AccessKey")
        request.setValue ("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = encoded
        
        let task =  URLSession.shared.dataTask(with: request) {
            data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            do {
                let decodedResponse = try JSONDecoder().decode(WelcomePostCall.self, from: data)
                self.accessString  = decodedResponse.accessToken
                print(decodedResponse.accessToken)
                
            } catch {
                print("Unexpected error: \(error).")
                print("failed")
            }
        }
        task.resume()
    }
    
    func loadData ()  async {
        
        guard let url = URL(string: "http://84.201.188.117:5003/api/v3/ibonus/generalinfo/\(accessString)") else {
            print ("invalid URL")
            return
        }
        
        var request =  URLRequest(url: url)
        request.setValue ("891cf53c-01fc-4d74-a14c-592668b7a03c", forHTTPHeaderField: "AccessKey")
        
        do {
            let (data, _) = try  await URLSession.shared.data(for: request)
            
            let str = String(decoding: data, as: UTF8.self)
            print(str)
            let decodedResult = try JSONDecoder().decode(Welcome.self, from: data)
            DispatchQueue.main.async {
                self.result = decodedResult
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let dateString = decodedResult.data.dateBurning
                let date = dateFormatter.date(from: dateString)!
                dateFormatter.dateFormat = "dd.MM"
                let str = dateFormatter.string(from: date)
                self.dateToStr = str
            }
            print(decodedResult)
            
        }catch{
            print ("invalid data")
            print("Unexpected error: \(error).")
        }
    }
}
