//
//  BonusView.swift
//  iProBonus
//
//  Created by Artem Soloviev on 29.06.2023.
//

import SwiftUI

struct BonusView: View {
    @ObservedObject var apiCall: ApiCall
    var body: some View {
        HStack{
            VStack(alignment: .leading){
                Text("\(apiCall.result?.data.currentQuantity ?? 0) бонусов")
                    .font(.custom("Open Sans", size: 24))
                    .fontWeight(.bold)
                HStack{
                    Text("\(apiCall.dateToStr) сгорит")
                        .font(.custom("Open Sans", size: 16))
                        .foregroundColor(.gray)
                    Image("Fier")
                        .resizable()
                        .frame(width: 13, height: 17, alignment: .center)
                    Text("\(apiCall.result?.data.forBurningQuantity ?? 0) бонусов" )
                        .font(.custom("Open Sans", size: 16))
                        .foregroundColor(.gray)
                }
            }.padding(.trailing)
            Spacer()
            VStack(alignment: .trailing){
                Button {
                    
                }label: {
                    Image(systemName: "chevron.right.circle")
                        .font(.largeTitle)
                        .foregroundColor(Color("ArrowButton"))
                }
            }.padding(.leading)
        }.padding()
            .background( RoundedRectangle(cornerRadius: 20)
                .fill(.white)
                .shadow(color: .black.opacity(0.2), radius: 8, x: 2, y: 2))
            .padding(.horizontal)
    }
}

//struct BonusView_Previews: PreviewProvider {
//    static var previews: some View {
//        BonusView()
//    }
//}
