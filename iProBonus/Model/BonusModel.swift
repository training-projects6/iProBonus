//
//  BonusModel.swift
//  iProBonus
//
//  Created by Artem Soloviev on 23.06.2023.
//

import Foundation


struct Welcome: Codable {
    var resultOperation: ResultOperation
    var data: DataClass
}

struct DataClass: Codable {
    var typeBonusName: String
    var currentQuantity: Int
    var forBurningQuantity: Int
    var dateBurning: String
}

struct ResultOperation: Codable {
    var status: Int
    var message: String
    var messageDev: String?
    var codeResult: Int
    var duration: Int
    var idLog: String
}
