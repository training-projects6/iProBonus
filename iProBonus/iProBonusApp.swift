//
//  iProBonusApp.swift
//  iProBonus
//
//  Created by Artem Soloviev on 23.06.2023.
//

import SwiftUI

@main
struct iProBonusApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
