//
//  ContentView.swift
//  iProBonus
//
//  Created by Artem Soloviev on 23.06.2023.
//

import SwiftUI
import Bonus
import ApiCall

struct ContentView: View {
    
    @StateObject var apiCall = ApiCall(result: nil, dateToStr: "")
    
    var body: some View {
        NavigationStack{
            VStack {
                
                ZStack(alignment: .top) {
                    
                    Rectangle()
                        .fill(
                            LinearGradient(
                                stops: [
                                    Gradient.Stop(color: Color("Red"), location: 0.00),
                                    Gradient.Stop(color: Color("Red2"), location: 1.00),
                                ],
                                startPoint: UnitPoint(x: 0.5, y: 0),
                                endPoint: UnitPoint(x: 0.5, y: 1)
                            )
                        )
                        .frame( height: 148)
                        .padding(.top, 50)
                    
                    BonusView(apiCall: apiCall)
                }.padding(.top, 20)
                Spacer()
                    .toolbar{
                        ToolbarItem(placement: .navigationBarLeading) {
                            Text("логотип")
                                .font(.custom("Open Sans", size: 16))
                        }
                        ToolbarItem(placement: .navigationBarTrailing)  {
                            Image("Logo")
                                .resizable()
                                .frame(width: 24, height: 24, alignment: .center)
                        }
                    }
            }
        }
    }
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(apiCall: ApiCall())
    }
}
